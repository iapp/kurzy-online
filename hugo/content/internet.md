---
title: "Internet"
description: "Online, ale i prezenční kurzy pro nejširší veřejnost"
draft: false
menu: main
weight: 2
keywords: kurzy online, G-Suite, bezpečnost
---

Jak přežít internet   (1,5h)
============================
![internet survival](/images/work_internet.png#floatleft)

Internet je všude, používáme ho pro komunikaci se svými blízkými, s úřady i bankou. Je to zdroj vědění, zábavy, ale i možných potíží. Nabízím Vám řidičský kurz na Váš internetový prohlížeč, budeme se zabývat vyhledáváním a zapamatováním si informací, ale především Vaší bezpečností...


Kurz pro vás tvoří a vede [Marek Sirový](/lektori/#marek-sirový)

--------
--------

GSuite/Gmail chytře  (3-5h)
===========================

![Gmail chytře](/images/gmail_core_apps.png#floatright-big)

Gmail může mít každý zdarma a mnoho z Vás už ho dokonce má. Gmail není jen email, je to velmi promyšlená sada pomůcek pro každý den Vašeho osobního i pracovního života. Lze s ním chodit nakupovat, organizovat dovolenou, pracovat na domácích úkolech, spravovat recepty po babičce nebo domácí rozpočet. S pomocí pouhého GSuite/gmailu můžete řídit malou i větší firmu řemeslníků (dnes má každý chytrý telefon), penzion, ale uděláte s ním velkou parádu i při digitalizaci Vaší školy, úřadu, nebo jen týmu kolegů.

Nabízím Vám úvodní kurz, kde nebudeme bezcílně proklikávat aplikace, ale kde si libovolnou z  výše uvedených situací přímo vyzkoušíme.


Kurz pro vás tvoří a vede [Marek Sirový](/lektori/#marek-sirový)

--------
--------
