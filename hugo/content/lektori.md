---
title: "Lektoři"
description: ""
images: []
draft: false
menu: main
weight: 10
---

## Markéta Kreuzingerová

![Markéta Kreuzingerová](/images/marketa.jpg#floatright)


Angličtina je nedílnou součástí mého života už od mých 13i let a s naprostou upřímností můžu říct, že tento jazyk miluju. Nyní využívám své zkušenosti k tomu, abych pomohla lidem překonat obavy a skutečně začít angličtinu používat. Neschovávám se za papírové certifikáty, zato mám opravdové zkušenosti z každodenního života.


### Proč právě já?
  * Studovala jsem na dvojjazyčném gymnáziu v Olomouci, kde výuka většiny předmětů probíhala v AJ
  * Žila jsem, studovala, a pracovala v USA po dobu téměř 6i let
    * --  Doučovala jsem americké spolužáky z různých předmětů (např. ekonomie)
    * --  Používala jsem Business/Financial English na každodenní bázi v rámci studia i zaměstnání
  * Pracovala jsem přes 3 roky v korporátním sektoru - bankovnictví
    * --  Zpracovávala jsem reporting, překlady, korekce textů pro zahraniční spolupráci
  * Angličtinu učím nebo doučuji již od svých 15i let

Zde přidávám ještě odkazy na:
  * můj [linkedin](https://www.linkedin.com/in/marketa-kreuzingerova-705ba1b0/)
  * mé [talenty](https://high5test.com/test/result-your-friend/NzAzOTEx)

Napište mi na marketakreuz@gmail.com, nebo volejte na telefon 705226590

-----------
-----------

## Marek Sirový

Jsem prakticky zaměřený minimalista. Rád poznávám nové lidi a řeším zajímavé problémy. Baví mě hledat způsoby, jak objasnit složité skutečnosti jednoduckým způsobem. Cílem mých kurzů je pomoci Vám s řešením Vašich skutečných problémů.

![Marek Sirový](/images/marek.jpg#floatleft)

Mám relativně široký záběr a prošel jsem dlouhou cestu z malé vesnice ve Středočeském kraji až do známých firem jako je Kiwi, Zonky, 4.house, Easysoftware, Twisto, Nethost atp. Strávil jsem mnoho měsíců prací na dálku a home office, zabývám se osobní i týmovou efektivitou, mám dlouholeté zkušenosti v oblasti IT (od roku 2005), baví mě "praktická" sociologie a komplexní systémy. V posledních letech se věnuji návrhu a stavbě cloudových řešení, projektovému řízení a týmové efektivitě nejen v distribuovaných týmech.

Snažím se vést prakticky zaměřená školení pro malé skupiny, nebo jednotlivce.


Zde přidávám ještě odkazy na:
  * můj [linkedin](https://www.linkedin.com/in/msirovy/)
  * mé [talenty](https://high5test.com/test/result-your-friend/NzAzODE0)

a pár ukázek mé IT práce: 
  * demo [IaaC pro startupy](https://github.com/theztd/startup-infra-docker)
  * demo [primitivni Flask applikace s CI/CD a metrikami](https://github.com/theztd/flaskapp-prom)
  * ...


Kontaktovat mě můžete na msirovy @ gmail.com

-----------
-----------

