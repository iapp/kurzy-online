---
title: "Efektivita"
description: "Kurzy online na témata jako efektivita z homeoffice, práce v distribuovaných týmech atd."
draft: false
menu: main
weight: 2
keywords: kurzy online, efektivita, homeoffice
---

Efektivní práce (na dálku i v kanceláři)    (2-3h)
==================================================

![efektivita](/images/efektivni_prace.png#center)
V 8 ráno jste dorazili do práce a najednou je oběd, ale vy jste kromě kafe zatím skoro nic nestihli. To se stává každému, ale nemuselo by. Ukážu Vám praktické a velmi jednoduché způsoby, jak za 4 hodiny denně stihnout to, co ostatní dělají 8 hodin. Získáte tak prostor na soukromé záležitosti, nebo na vlastní rozvoj.

Kurz je zaměřený na pochopení jak sami fungujeme a ovládnutí praktických fíglů, jak stíhat a nekončit každý den nevyždímaný. Kurz má celkem pevnou strukturu, ale je v něm dostatek prostoru pro diskuzi nad individuálními potřebami účastníků.


Kurz pro vás tvoří a vede [Marek Sirový](/lektori/#marek-sirový)

--------
--------

Naskočte do ZTD (3-5h)
======================
![ZTD](/images/ztd.png#floatleft)
Je to velmi jednoduchý systém k adoptování, který na rozdíl od mnohem známějšího GTD lze adoptovat postupně. To dělá celý systém aplikovatelným i pro méně organizované lidi jako jsem třeba i já. Systém ZTD nepředpokládá robotické dodržování všech bodů za každé situace, ale dává prostor k upravám každému na míru.

**V kurzu**:

 * projdeme základní informace o tom jak fungujeme a proč nějaký systém potřebujem
 * projdeme systém ZTD krok za krokem a povíme si co nám pomáhá řešit
 * ukážeme si jak lze systém adoptovat do našeho života po kouskách a že i to může někomu stačit
 * na konec zbyde prostor pro diskuze a praktické ukázky jak lze systém napasovat na nástroje a situace.

Cílem tohoto kurzu je pomoci účastníkům s aplikací systému ZTD do vlastního života...

Kurz pro vás tvoří a vede [Marek Sirový](/lektori/#marek-sirový)

--------
--------

Komunikace v distribuovaných týmech  (2-5h)
===========================================

Základem každého týmu je komunikace. Pokud komunikace nefunguje, ani nejmodernější agilní metodologie dlouhodobě nezvýší spokojenost týmu a už vůbec ne jeho výkonost.
![komunikace](/images/comunication.png#floatright)
Proto jsem se rozhodl tomuto tématu věnovat celý kurz, kde prakticky využijeme poznatky z mnoha zdrojů a společností, které v distribuovaných týmech dosahují vynikajících výkonů.


Během tohoto kurzu projdeme druhy komunikace a jejich správné uplatnění. Z toho dále odvodíme, co bychom měli při volbě nástrojů pro každý typ komunikace vyžadovat a sestavíme 5 jednoduchých principů, jak mezi nimi vybírat a co dodržovat...


Z kurzu si odnesete praktické znalosti jak komunikovat efektivně online mezi pár kolegy, nebo v desítkách lidí, naučíte se správně zvolit nástroj a pokud ste leader, získáte představu jak řídit distribuované týmy...


Kurz pro vás tvoří a vede [Marek Sirový](/lektori/#marek-sirový)

--------
--------

GSuite/Gmail chytře  (3-5h)
===========================

![Gmail chytře](/images/gmail_core_apps.png#floatright-big)

Gmail může mít každý zdarma a mnoho z Vás už ho dokonce má. Gmail není jen email, je to velmi promyšlená sada pomůcek pro každý den Vašeho osobního i pracovního života. Lze s ním chodit nakupovat, organizovat dovolenou, pracovat na domácích úkolech, spravovat recepty po babičce nebo domácí rozpočet. S pomocí pouhého GSuite/gmailu můžete řídit malou i větší firmu řemeslníků (dnes má každý chytrý telefon), penzion, ale uděláte s ním velkou parádu i při digitalizaci Vaší školy, úřadu, nebo jen týmu kolegů.

Nabízím Vám úvodní kurz, kde nebudeme bezcílně proklikávat aplikace, ale kde si libovolnou z výše uvedených situací přímo vyzkoušíme. Pokud tento kurz navštívíte jako firma/tým, můžeme v jeho rámci nastavit základy vaší další spolupráce, nebo ho pojmout formou workshopu/simulace.


Kurz pro vás tvoří a vede [Marek Sirový](/lektori/#marek-sirový)

--------
--------
